#!/usr/bin/zsh

REPO_DIR=${0:a:h}

ALACRITTY_REPO_FILE=$REPO_DIR/config_files/alacritty.yml
ALACRITTY_LOCAL_FILE=$HOME/.config/alacritty/alacritty.yml

TMUX_REPO_FILE=$REPO_DIR/config_files/tmux.conf
TMUX_LOCAL_FILE=$HOME/.tmux.conf

ZSH_REPO_FILE=$REPO_DIR/config_files/zshrc
ZSH_LOCAL_FILE=$HOME/.zshrc

P10K_REPO_FILE=$REPO_DIR/config_files/p10k.zsh
P10K_LOCAL_FILE=$HOME/.p10k.zsh

echo "Patching alacritty configuration..."
if [ ! -d ${ALACRITTY_LOCAL_FILE:h} ]
then
  echo "    Config directory for alacritty not found... so creating it..."
  mkdir -p ${ALACRITTY_LOCAL_FILE:h}
fi
ln -f $ALACRITTY_REPO_FILE $ALACRITTY_LOCAL_FILE

echo "Patching tmux configuration..."
ln -f $TMUX_REPO_FILE $TMUX_LOCAL_FILE

echo "Patching zsh configuration..."
ln -f $ZSH_REPO_FILE $ZSH_LOCAL_FILE

echo "Patching p10k configuration..."
ln -f $P10K_REPO_FILE $P10K_LOCAL_FILE
